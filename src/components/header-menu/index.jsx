import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import { Menu } from "antd";

const HeaderMenu = () => {
  const history = useHistory();

  const [current, setCurrent] = useState("login");

  const handleClick = (evt) => {
    setCurrent(evt.key);
  };
  return (
    <Menu onClick={handleClick} selectedKeys={current} mode={"horizontal"}>
      <Menu.Item key="cadastro" onClick={() => history.push("/cadastro")}>
        Cadastro
      </Menu.Item>

      <Menu.Item key="login" onClick={() => history.push("/")}>
        Login
      </Menu.Item>
    </Menu>
  );
};

export default HeaderMenu;
