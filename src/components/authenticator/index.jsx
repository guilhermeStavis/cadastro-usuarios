import React, { useState, useEffect } from "react";
import { Route, Switch, useHistory } from "react-router-dom";

import Login from "../../pages/login";
import Users from "../../pages/users";
import UserForm from "../../pages/user-form";
import FeedbackList from "../../pages/feedback-list";
import FeedbackForm from "../../pages/feedback-form";

import HeaderMenu from "../header-menu";
import HeaderMenuLogged from "../header-menu-logged";

import axios from "axios";
const Authenticator = () => {
  const [isAuthenticated, setAuthentication] = useState(undefined);
  const history = useHistory();

  useEffect(() => {
    const token = window.localStorage.getItem("authToken");

    !token && setAuthentication(false);

    axios
      .get("https://ka-users-api.herokuapp.com/users", {
        headers: { Authorization: token },
      })
      .then(() => {
        setAuthentication(true);
        history.push("/users");
      })
      .catch(() => setAuthentication(false));
  }, [history, setAuthentication]);

  if (isAuthenticated === undefined) {
    return <div>Loading...</div>;
  }

  if (isAuthenticated === false) {
    return (
      <Switch>
        <Route exact path="/">
          <HeaderMenu />
          <Login setAuthentication={setAuthentication} />
        </Route>

        <Route path="/cadastro">
          <HeaderMenu />
          <UserForm />
        </Route>
      </Switch>
    );
  }
  return (
    <Switch>
      <Route exact path="/">
        <HeaderMenu />
        <Login setAuthentication={setAuthentication} />
      </Route>

      <Route path="/users">
        <HeaderMenuLogged setAuthentication={setAuthentication} />
        <Users />
      </Route>

      <Route path="/feedback-list/:user_id">
        <HeaderMenuLogged setAuthentication={setAuthentication} />
        <FeedbackList />
      </Route>

      <Route path="/feedback-form/:user_id">
        <HeaderMenuLogged setAuthentication={setAuthentication} />
        <FeedbackForm />
      </Route>
    </Switch>
  );
};

export default Authenticator;
