import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import { Menu } from "antd";

const HeaderMenuLogged = ({ setAuthentication }) => {
  const history = useHistory();

  const [current, setCurrent] = useState("alunos");

  const handleClick = (evt) => {
    setCurrent(evt.key);
  };
  return (
    <Menu onClick={handleClick} selectedKeys={current} mode={"horizontal"}>
      <Menu.Item key="alunos" onClick={() => history.push("/users")}>
        Alunos
      </Menu.Item>

      <Menu.Item
        key="login"
        onClick={() => {
          window.localStorage.clear();
          setAuthentication(false);
          history.push("/");
        }}
      >
        Logout
      </Menu.Item>
    </Menu>
  );
};

export default HeaderMenuLogged;
