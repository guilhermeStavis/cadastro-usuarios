import styled from "styled-components";

export const Interface = styled.div`
  @import url("https://fonts.googleapis.com/css2?family=Ubuntu:ital@1&display=swap");
  background-color: #822020;
  height: 100vh;
  display: flex;
  align-items: center;
  font-family: "Ubuntu", sans-serif;

  form {
    box-sizing: border-box;
    background-color: #fff;
    width: 70vw;
    height: 32vh;
    margin: 0 auto;
    transform: translate(0, -50%);
    border-radius: 5px;
  }

  form h1 {
    color: #822020;
  }

  form button {
    background-color: #822020;
    color: white;
    border: none;
    font-size: 1.2rem;
    padding: 4px 15px;
    border-radius: 5px;
  }

  @media screen and (min-width: 768px) {
    form {
      width: 30vw;
      height: 40vh;
    }
  }
`;
