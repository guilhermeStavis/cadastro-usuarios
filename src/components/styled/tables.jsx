import styled from "styled-components";

export const Interface = styled.div`
  @import url("https://fonts.googleapis.com/css2?family=Ubuntu:ital@1&display=swap");
  background-color: #822020;
  height: 130vh;
  font-family: "Ubuntu", sans-serif;

  h1 {
    color: white;
  }

  button {
    background-color: #d33737;
    border: none;
    font-size: 1.2rem;
    padding: 4px 15px;
    border-radius: 5px;
  }

  button a {
    color: white;
  }
`;

export const Tabela = styled.div`
  margin: 0 auto;
  width: 80vw;
`;
