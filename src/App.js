import "./App.css";
import "antd/dist/antd.css";
import Authenticator from "./components/authenticator";

function App() {
  return (
    <div className="App">
      <Authenticator />
    </div>
  );
}

export default App;
