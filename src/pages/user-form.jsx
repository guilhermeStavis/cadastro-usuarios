import React from "react";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import axios from "axios";

import styled from "styled-components";

const UserForm = () => {
  const history = useHistory();
  const scheme = yup.object().shape({
    name: yup
      .string()
      .matches(/[A-Za-z]\s[A-Za-z]/, "Formato Inválido")
      .required("Campo Obrigatório"),
    user: yup
      .string()
      .min(6, "Mínimo de 6 caractéres")
      .required("Campo Obrigatório"),
    email: yup
      .string()
      .email("Formato de Email inválido")
      .required("Campo Obrigatório"),
    password: yup
      .string()
      .min(6, "Mínimo de 6 caracteres")
      .matches(
        /^((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
        "Senha deve conter ao menos uma letra maiúscula, um número e um caracter especial"
      )
      .required("Campo Obrigatório"),
    password_confirmation: yup
      .string()
      .oneOf([yup.ref("password")], "Senhas diferentes")
      .required("Campo Obrigatório"),
  });

  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(scheme),
  });

  const handleForm = (data) => {
    console.log(data);

    axios
      .post("https://ka-users-api.herokuapp.com/users", { user: data })
      .then(() => history.push("/"))
      .catch((res) => console.log(res));
  };

  return (
    <Interface>
      <form onSubmit={handleSubmit(handleForm)}>
        <h1>Cadastro de Usuário</h1>
        <div>
          <label for="name">*Nome: </label>
          <input type="text" name="name" id="name" ref={register} />
          <p>{errors.name?.message}</p>
        </div>
        <div>
          <label for="user">*Usuário: </label>
          <input type="text" name="user" id="user" ref={register} />
          <p>{errors.user?.message}</p>
        </div>
        <div>
          <label for="email">*Email: </label>
          <input type="text" name="email" id="email" ref={register} />
          <p>{errors.email?.message}</p>
        </div>
        <div>
          <label for="password">*Senha: </label>
          <input type="text" name="password" id="password" ref={register} />
          <p>{errors.password?.message}</p>
        </div>
        <div>
          <label for="confirmPassword">*Confirmar Senha: </label>
          <input
            type="text"
            name="password_confirmation"
            id="confirmPassword"
            ref={register}
          />
          <p>{errors.password_confirmation?.message}</p>
        </div>

        <button type="submit">Cadastrar</button>
      </form>
    </Interface>
  );
};

export default UserForm;

const Interface = styled.div`
  @import url("https://fonts.googleapis.com/css2?family=Ubuntu:ital@1&display=swap");
  background-color: #822020;
  height: 100vh;
  display: flex;
  align-items: center;
  font-family: "Ubuntu", sans-serif;

  form {
    box-sizing: border-box;
    background-color: #fff;
    width: 70vw;
    height: 70vh;
    margin: 0 auto;
    border-radius: 5px;
    display: flex;
    flex-direction: column;
    align-items: flex-end;
  }

  form h1 {
    color: #822020;
    align-self: center;
  }

  form input {
    border: 1px solid gray;
    border-radius: 3px;
  }

  form button {
    background-color: #822020;
    color: white;
    border: none;
    font-size: 1.2rem;
    padding: 4px 15px;
    border-radius: 5px;
    align-self: center;
  }

  @media screen and (min-width: 768px) {
    form {
      width: 30vw;
      height: 60vh;
    }

    form input {
      margin-right: 40px;
      margin-bottom: 10px;
    }
  }
`;
