import React, { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";

import { Table } from "antd";

import axios from "axios";

import { Interface, Tabela } from "../components/styled/tables";

const FeedbackList = () => {
  const params = useParams();

  const [dataSource, setDataSource] = useState([]);
  const [columns] = useState([
    {
      title: "Id",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Comment",
      dataIndex: "comment",
      key: "comment",
    },
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Grade",
      dataIndex: "grade",
      key: "grade",
    },
  ]);

  useEffect(() => {
    axios
      .get(
        `https://ka-users-api.herokuapp.com/users/${params.user_id}/feedbacks`,
        {
          headers: { Authorization: window.localStorage.getItem("authToken") },
        }
      )
      .then((res) => setDataSource(res.data));
  });

  return (
    <Interface>
      <h1>Feedbacks</h1>
      {params.id}
      <Tabela>
        <Table dataSource={dataSource} columns={columns} />
      </Tabela>
      <button>
        <Link to={`/feedback-form/${params.user_id}`}>New Feedback</Link>
      </button>
    </Interface>
  );
};

export default FeedbackList;
