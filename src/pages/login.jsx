import React from "react";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import axios from "axios";

import { Interface } from "../components/styled/forms";

const Login = ({ setAuthentication }) => {
  const history = useHistory();
  const scheme = yup.object().shape({
    user: yup.string().required("Campo Obrigatório"),
    password: yup.string().required("Campo obrigatório"),
  });
  const { register, handleSubmit, setError, errors } = useForm({
    resolver: yupResolver(scheme),
  });

  const handleForm = (data) => {
    axios
      .post("https://ka-users-api.herokuapp.com/authenticate", { ...data })
      .then((res) => {
        window.localStorage.setItem("authToken", res.data.auth_token);
        setAuthentication(true);
        history.push("/users");
      })
      .catch(() => {
        setError("password", { message: "Credenciais inválidas" });
      });
  };
  return (
    <Interface>
      <form onSubmit={handleSubmit(handleForm)}>
        <h1>Login</h1>
        <div>
          <input placeholder="Usuário" name="user" ref={register} />
          <p style={{ color: "red" }}>{errors.user?.message}</p>
        </div>

        <div>
          <input placeholder="Senha" name="password" ref={register} />
          <p>{errors.password?.message}</p>
        </div>

        <button type="submit">Entrar</button>
      </form>
    </Interface>
  );
};

export default Login;
