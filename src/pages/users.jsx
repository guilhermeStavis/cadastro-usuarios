import React, { useEffect, useState } from "react";

import { Link } from "react-router-dom";

import { Table } from "antd";
import axios from "axios";

import { Interface, Tabela } from "../components/styled/tables";

const Users = () => {
  const [dataSource, setDataSource] = useState([]);
  const [columns] = useState([
    {
      title: "Id",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "User",
      dataIndex: "user",
      key: "user",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Feedback",
      key: "feedback",
      render: (text, record) => (
        <Link to={`/feedback-list/${record.id}`}>Go to Feedbacks</Link>
      ),
    },
  ]);

  useEffect(() => {
    axios
      .get("https://ka-users-api.herokuapp.com/users", {
        headers: { Authorization: window.localStorage.getItem("authToken") },
      })
      .then((res) => {
        setDataSource(res.data);
      });
  });

  return (
    <Interface>
      <h1>Alunos</h1>
      <Tabela>
        <Table dataSource={dataSource} columns={columns} />
      </Tabela>
    </Interface>
  );
};

export default Users;
