import React from "react";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

import axios from "axios";
import { useParams, useHistory } from "react-router-dom";

import { Interface } from "../components/styled/forms";

const FeedbackForm = () => {
  const params = useParams();
  const history = useHistory();

  const scheme = yup.object().shape({
    name: yup.string().required("Campo Obrigatório"),
    comment: yup.string().required("Campo Obrigatório"),
    grade: yup.string().required("Campo Obrigatório"),
  });

  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(scheme),
  });

  const handleForm = (data) => {
    axios
      .post(
        `https://ka-users-api.herokuapp.com/users/${params.user_id}/feedbacks`,
        {
          feedback: { ...data },
        },
        {
          headers: { Authorization: window.localStorage.getItem("authToken") },
        }
      )
      .then(() => {
        history.push(`/feedback-list/${params.user_id}`);
      });
  };

  return (
    <Interface>
      <form onSubmit={handleSubmit(handleForm)}>
        <h1>New Feedback</h1>
        <div>
          <input placeholder="name" name="name" ref={register} />
          <p>{errors.name?.message}</p>
        </div>

        <div>
          <input placeholder="comment" name="comment" ref={register} />
          <p>{errors.comment?.message}</p>
        </div>

        <div>
          <input placeholder="grade" name="grade" ref={register} />
          <p>{errors.grade?.message}</p>
        </div>
        <button type="submit">Enviar</button>
      </form>
    </Interface>
  );
};

export default FeedbackForm;
